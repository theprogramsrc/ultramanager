package me.theprogramsrc.ultramanager;

import me.theprogramsrc.ultramanager.object.UServer;
import me.theprogramsrc.ultramanager.object.User;
import me.theprogramsrc.ultramanager.storage.UserStorage;
import org.bukkit.entity.Player;

import java.util.UUID;

public interface IManager{

    void addUser(User user);

    void remUser(User user);

    void saveUser(User user);

    User getUser(Player player);

    User getUser(UUID uuid);

    User getRandomUser();

    User[] getUsers();

    void saveServer(UServer UServer);

    UServer getUServer();

    UserStorage getUserStorage();

}
