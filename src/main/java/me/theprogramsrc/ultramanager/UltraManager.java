package me.theprogramsrc.ultramanager;

import me.theprogramsrc.tpsapi.TheProgramSrcClass;
import me.theprogramsrc.tpsapi.ownOptions.files.Config;
import me.theprogramsrc.tpsapi.programmingUtils.MathUtil;
import me.theprogramsrc.ultramanager.cmds.UMC;
import me.theprogramsrc.ultramanager.cmds.UManager;
import me.theprogramsrc.ultramanager.listeners.UserListeners;
import me.theprogramsrc.ultramanager.object.UServer;
import me.theprogramsrc.ultramanager.object.User;
import me.theprogramsrc.ultramanager.storage.UserStorage;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public final class UltraManager extends JavaPlugin implements IManager{

    public static TheProgramSrcClass tps;
    public static IManager i;
    private UserStorage userStorage;

    @Override
    public void onEnable(){
        tps = new TheProgramSrcClass(this,"UltraManager","TheProgramSrc","&bU&5Manager&7>&r ","en",false,false,false);
        i = this;
        tps.log(tps.getLanguage().equalsIgnoreCase("es") ? "&aHabilitando plugin" : "&aEnabling plugin");
        this.userStorage = new UserStorage();
        tps.getSQLConnection().connect(false);
        new UserListeners();
        new UManager();
        new UMC();
        tps.log(tps.getLanguage().equalsIgnoreCase("es") ? "&aPlugin habilitado" : "&aPlugin enabled");
    }

    @Override
    public void onDisable(){
        tps.log(tps.getLanguage().equalsIgnoreCase("es") ? "&cDeshabilitando plugin" : "&cDisabling plugin");
        tps.setStatsEnabled(false);
        tps.getSQLConnection().disconnect(false);
        tps.log(tps.getLanguage().equalsIgnoreCase("es") ? "&cPlugin deshabilitado" : "&cPlugin disabled");
    }

    public static IManager getInstance(){
        return i;
    }

    @Override
    public void addUser(User user) {
        this.userStorage.add(user);
    }

    @Override
    public void remUser(User user) {
        this.userStorage.rem(user);
    }

    @Override
    public void saveUser(User user) {
        this.userStorage.save(user);
    }

    @Override
    public User getUser(Player player) {
        return this.getUser(player.getUniqueId());
    }

    @Override
    public User getUser(UUID uuid) {
        return this.userStorage.getUser(uuid);
    }

    @Override
    public User getRandomUser() {
        return this.getUsers()[MathUtil.fairRoundedRandom(0, this.getUsers().length)];
    }

    @Override
    public User[] getUsers() {
        return this.userStorage.getUsers();
    }

    @Override
    public void saveServer(UServer UServer) {
        Config config = tps.getConfig();
        config.add("ServerSettings", UServer.serialize());
    }

    @Override
    public UServer getUServer() {
        Config config = tps.getConfig();
        UServer s = UServer.deserialize(config.getString("ServerSettings"));
        return s;
    }

    @Override
    public UserStorage getUserStorage() {
        return this.userStorage;
    }
}
