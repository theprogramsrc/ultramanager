package me.theprogramsrc.ultramanager.cmds;

import me.theprogramsrc.tpsapi.TPS;
import me.theprogramsrc.tpsapi.gameManagement.commands.Command;
import me.theprogramsrc.tpsapi.gameManagement.commands.ICommand;
import me.theprogramsrc.tpsapi.gameManagement.commands.Result;
import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.inv.Overview;
import me.theprogramsrc.ultramanager.object.User;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UManager extends TPS implements ICommand {

    public UManager() {
        super(UltraManager.tps);
        this.registerCommand(this);
    }

    @Override
    public Result onPlayerExecute(Player player, String[] strings) {
        User u = UltraManager.i.getUser(player);
        if(u != null){
            if(u.isAdmin()){
                new Overview(u);
            }else{
                return Result.NO_PERMISSION;
            }
        }
        return Result.COMPLETED;
    }

    @Override
    public Result onConsoleExecute(CommandSender commandSender, String[] strings) {
        return Result.NOT_SUPPORTED;
    }

    @Override
    public Command getCommand() {
        Command cmd = new Command("umanager");
        cmd.setDescription(getLanguage().equalsIgnoreCase("es") ? "Comando principal" : "Principal command");
        cmd.addAlias("um");
        cmd.addAlias("ultramanager");
        cmd.addAlias("ultram");
        return cmd;
    }

}