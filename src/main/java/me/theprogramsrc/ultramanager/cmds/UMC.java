package me.theprogramsrc.ultramanager.cmds;

import me.theprogramsrc.tpsapi.gameManagement.commands.Command;
import me.theprogramsrc.tpsapi.gameManagement.commands.ICommand;
import me.theprogramsrc.tpsapi.gameManagement.commands.Result;
import me.theprogramsrc.tpsapi.programmingUtils.Utils;
import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.cmds.subCommands.AddAdmin;
import me.theprogramsrc.ultramanager.cmds.subCommands.RemAdmin;
import me.theprogramsrc.ultramanager.cmds.subCommands.UMCSubCmd;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class UMC implements ICommand {

    private static UMCSubCmd[] subCmds = new UMCSubCmd[]{ new AddAdmin(), new RemAdmin()};

    public UMC(){
        UltraManager.tps.registerCommand(this);
    }

    @Override
    public Result onPlayerExecute(Player player, String[] args) {
        return this.onConsoleExecute(player, args);
    }

    @Override
    public Result onConsoleExecute(CommandSender commandSender, String[] args) {
        if(args.length == 0){
            this.showHelp(commandSender);
        }else{
            for(UMCSubCmd subCmd : subCmds){
                String var0 = subCmd.getSubCommand().toLowerCase();
                String var1 = args[0].toLowerCase();
                if(var0.equalsIgnoreCase(var1)){
                    String[] subCmdArgs = Arrays.copyOfRange(args,1,args.length);
                    boolean execute = subCmd.execute(commandSender, subCmdArgs);
                    if(!execute){
                        Utils.sendMessage(commandSender, "");
                        Utils.sendMessage(commandSender, "&7Correct usage: /umc "+subCmd.getSubCommand());
                    }
                }
                return Result.COMPLETED;
            }
            this.showHelp(commandSender);
        }
        return Result.COMPLETED;
    }

    @Override
    public Command getCommand() {
        return new Command("umc").setPermission("ultramanager.admin").setDescription(UltraManager.tps.getLanguage().equalsIgnoreCase("es") ? "Comando Principal" : "Main Command");
    }

    public void showHelp(CommandSender sender){
        if(UltraManager.tps.getLanguage().equalsIgnoreCase("es")){
            Utils.sendMessage(sender, "");
            Utils.sendMessage(sender, "&eLista de Comandos:");
            for(UMCSubCmd subCmd : subCmds){
                Utils.sendMessage(sender, "&7/&eumc "+subCmd.getSubCommand()+"&7 | &8Uso: &e"+subCmd.getUsage());
            }
        }else{
            Utils.sendMessage(sender, "");
            Utils.sendMessage(sender, "&eList of Commands:");
            for(UMCSubCmd subCmd : subCmds){
                Utils.sendMessage(sender, "&7/&eumc "+subCmd.getSubCommand()+"&7 | &8Usage: &e"+subCmd.getUsage());
            }
        }
    }
}
