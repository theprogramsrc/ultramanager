package me.theprogramsrc.ultramanager.object;

import me.theprogramsrc.tpsapi.gameManagement.player.PlayerLocation;
import me.theprogramsrc.ultramanager.UltraManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class User{

    private UUID uuid;
    private boolean admin;
    private String nick;
    private PlayerLocation lastLocation;

    public User(UUID uuid, boolean admin, String nick){
        this.uuid = uuid;
        this.admin = admin;
        this.nick = nick;
    }

    public void setLastLocation(PlayerLocation lastLocation) { this.lastLocation = lastLocation; }

    public void setAdmin(boolean admin) { this.admin = admin; }

    public User(UUID uuid, String nick){ this(uuid,false,nick); }

    public User(Player player, boolean admin, String nick){ this(player.getUniqueId(),admin,nick); }

    public User(Player player, boolean admin){ this(player,admin,player.getDisplayName()); }

    public User(Player player){ this(player, false); }

    public UUID getUuid(){ return this.uuid; }

    public boolean isAdmin(){ return this.admin; }

    public String getNick(){ return this.nick; }

    public String getName(){ return this.getPlayer() != null ? this.getPlayer().getName() : this.getNick(); }

    public void setNick(String nick){ this.nick = nick; }

    public Player getPlayer(){ return Bukkit.getPlayer(this.uuid); }

    public boolean isOnline(){ return this.getPlayer() != null && this.getPlayer().isOnline(); }

    public PlayerLocation getLocation(){ return this.getPlayer() != null ? new PlayerLocation(this.getPlayer()) : this.getLastLocation(); }

    public PlayerLocation getLastLocation(){ return this.lastLocation; }

    public String serialize(){ return this.uuid.toString()+"~"+this.admin +"~"+this.nick+"~"+PlayerLocation.serialize(this.getLastLocation()); }

    public static User deserialize(String serialized){
        String[] serializedUser = serialized.split("~");
        UUID uuid = UUID.fromString(serializedUser[0]);
        Boolean admin = Boolean.parseBoolean(serializedUser[1]);
        String nick = serializedUser[2];
        String lastLoc = serializedUser[3];
        User u = new User(uuid,admin,nick);
        u.setLastLocation(PlayerLocation.deserialize(lastLoc));
        return u.clone();
    }

    public User clone(){ return new User(this.getUuid(),this.isAdmin(),this.getNick()); }

    public boolean equals(User otherUser){ return (this.uuid == otherUser.getUuid()) && (this.isAdmin() == otherUser.isAdmin()) && (this.nick.equals(otherUser.getNick())); }

    public boolean equalsIgnoreCase(User otherUser){ return (this.uuid.toString().equalsIgnoreCase(otherUser.getUuid().toString())) && (Boolean.toString(this.isAdmin()).equalsIgnoreCase(Boolean.toString(otherUser.isAdmin()))) && (this.nick.equalsIgnoreCase(otherUser.getNick())); }

    public User save(){
        UltraManager.i.saveUser(this);
        return this;
    }
}
