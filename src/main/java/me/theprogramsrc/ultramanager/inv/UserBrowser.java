package me.theprogramsrc.ultramanager.inv;

import me.theprogramsrc.tpsapi.gameManagement.inventory.*;
import me.theprogramsrc.tpsapi.gameManagement.inventory.content.*;
import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.object.User;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class UserBrowser extends UpdatableInventory {

    private boolean onlineOnly = false;
    private Action action;

    public UserBrowser(User user, Action action){
        super(UltraManager.tps);
        this.action = action;
        this.loadUI(user.getPlayer());
    }

    @Override
    public String getInternalName() {
        return "UserBrowser";
    }

    @Override
    public String getTitle() { return this.getLanguage().equalsIgnoreCase("es") ? "&7> &5Usuarios" : "&7> &5Users"; }

    @Override
    public Integer getRows() { return 6; }

    @Override
    public boolean isCloseable() { return true; }

    @Override
    public void onUILoad(Player player, InventoryContents contents) {
        Pagination pagination = contents.pagination();
        if(this.onlineOnly){
            contents.set(new SlotPos(6, 3), this.getOfflineItem());
        }else{
            contents.set(new SlotPos(6, 3), this.getOnlineOnlyItem());
        }
        contents.set(new SlotPos(6,1), this.getBackItem(pagination));
        contents.set(new SlotPos(6,2), this.getBackItem());
        contents.set(new SlotPos(6,9), this.getNextItem(pagination));
        contents.set(new SlotPos(6,7), this.getCloseItem());
        pagination.setItemsPerPage(36);
        pagination.setItems(this.getUsersItems());
    }

    @Override
    public void onUIUpdate(Player player, InventoryContents contents) {
        Pagination pagination = contents.pagination();
        pagination.setItems(this.getUsersItems());
    }

    public ClickableItem getUserItem(final User user){
        UItem item = new UItem(UMaterial.PLAYER_HEAD).setSkullOwner(user.getName());
        item.setDisplayName("&2&l"+user.getName());
        item.setLore(
                "",
                "&7Admin: "+(user.isAdmin() ? (this.getLanguage().equalsIgnoreCase("es") ? "&aSi" : "&cNo") : (this.getLanguage().equalsIgnoreCase("es") ? "&aYes" : "&cNo")),
                "",
                "&7Nick: &r"+user.getNick(),
                "&7Coords: &5"+(user.isOnline() ? user.getLocation().getXYZ() : user.getLastLocation().getXYZ()),
                "&7"+(this.getLanguage().equalsIgnoreCase("es") ? "Mundo" : "World")+": &b"+(user.isOnline() ? user.getLocation().getXYZ() : user.getLastLocation().getXYZ()),
                ""
        );
        item.setAmount(1);
        ClickableItem clickableItem = new ClickableItem(item, e->this.action.run(UltraManager.getInstance().getUser(e.getWhoClicked().getUniqueId()), user));
        return clickableItem;
    }

    public ClickableItem[] getUsersItems(){
        ArrayList usersItemsArray = new ArrayList();
        User[] users =  Arrays.stream(UltraManager.i.getUsers()).filter(u-> !this.onlineOnly && u.isOnline()).toArray(User[]::new);
        for(User u : users){ usersItemsArray.add(this.getUserItem(u)); }
        ClickableItem[] items = ((ClickableItem[])usersItemsArray.toArray(new ClickableItem[usersItemsArray.size()]));
        return items;
    }

    public ClickableItem getBackItem(final Pagination pagination){ return new ClickableItem(new UItem(UMaterial.ARROW).setDisplayName("&a<--").setAmount(1),e->pagination.previous()); }

    public ClickableItem getBackItem(){ return new ClickableItem(new UItem(UMaterial.PLAYER_HEAD).setSkullOwner("MHF_ArrowLeft").setDisplayName("&a<--").setAmount(1), e-> new Overview(UltraManager.i.getUser(((Player)e.getWhoClicked())))); }

    public ClickableItem getNextItem(final Pagination pagination){ return new ClickableItem(new UItem(UMaterial.ARROW).setDisplayName("&a-->").setAmount(1),e->pagination.next()); }

    public ClickableItem getOnlineOnlyItem(){
        UItem item = new UItem(UMaterial.ENDER_EYE);
        item.setDisplayName(this.getLanguage().equalsIgnoreCase("es") ? "&aSolo Online" : "&aOnline Only");
        if(this.getLanguage().equalsIgnoreCase("es")){
            item.setLore(
                    "",
                    "&7Muestra solo los jugadores online"
            );
        }else{
            item.setLore(
                    "",
                    "&7Shows only online players"
            );
        }
        ClickableItem clickableItem = new ClickableItem(item, e-> this.onlineOnly = true);
        return clickableItem;
    }

    public ClickableItem getOfflineItem(){
        UItem item = new UItem(UMaterial.ENDER_PEARL);
        item.setDisplayName(this.getLanguage().equalsIgnoreCase("es") ? "&aTodos los jugadores" : "&aAll players");
        if(this.getLanguage().equalsIgnoreCase("es")){
            item.setLore(
                    "",
                    "&7Muestra todos los jugadores"
            );
        }else{
            item.setLore(
                    "",
                    "&7Shows all players"
            );
        }
        ClickableItem clickableItem = new ClickableItem(item, e-> this.onlineOnly = false);
        return clickableItem;
    }

    public ClickableItem getCloseItem(){ return ClickableItem.of(new UItem(UMaterial.REDSTONE_BLOCK).setDisplayName("&c&lX"), e->e.getWhoClicked().closeInventory()); }
}
