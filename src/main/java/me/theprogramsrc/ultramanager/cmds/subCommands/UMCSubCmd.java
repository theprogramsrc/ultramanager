package me.theprogramsrc.ultramanager.cmds.subCommands;

import me.theprogramsrc.tpsapi.TPS;
import me.theprogramsrc.ultramanager.UltraManager;
import org.bukkit.command.CommandSender;

public abstract class UMCSubCmd extends TPS {

    private String subCommand;
    private String usage;

    public UMCSubCmd(String subCommand, String usage){
        super(UltraManager.tps);
        this.subCommand = subCommand;
        this.usage = usage;
    }

    public abstract boolean execute(CommandSender sender, String[] args);

    public String getSubCommand() {
        return subCommand;
    }

    public String getUsage() {
        return usage;
    }
}
