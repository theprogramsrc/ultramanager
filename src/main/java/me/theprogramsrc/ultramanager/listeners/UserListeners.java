package me.theprogramsrc.ultramanager.listeners;

import me.theprogramsrc.tpsapi.TPS;
import me.theprogramsrc.tpsapi.gameManagement.customEvents.time.*;
import me.theprogramsrc.tpsapi.gameManagement.player.PlayerLocation;
import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.object.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class UserListeners extends TPS {

    public UserListeners(){ super(UltraManager.tps); }

    private ArrayList<User> users = new ArrayList<>();

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        User u = UltraManager.i.getUser(e.getPlayer());
        if(u != null){
            u.setLastLocation(new PlayerLocation(e.getPlayer()));
            UltraManager.i.saveUser(u);
            this.users.remove(u);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        User u = UltraManager.i.getUser(e.getPlayer());
        if(u != null){
            if(!this.users.contains(u)){
                this.users.add(u);
            }
        }else{
            User user = new User(e.getPlayer());
            user.save();
            this.users.add(user);
        }
    }

    @EventHandler
    public void syncUser(TimerEvent e){
        if(e.getTime() == Time.TICK){
            User[] users = Arrays.stream(UltraManager.i.getUsers()).filter(u-> u != null && u.isOnline()).toArray(User[]::new);
            for(User u : users){
                if(u != null){
                    u.getPlayer().setDisplayName(u.getNick());
                    PlayerLocation lastLocation = u.getLocation();
                    if(lastLocation != null){
                        u.setLastLocation(lastLocation);
                        u.save();
                    }
                }
            }
        }
    }
}
