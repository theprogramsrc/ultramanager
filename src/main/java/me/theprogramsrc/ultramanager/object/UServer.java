package me.theprogramsrc.ultramanager.object;

import me.theprogramsrc.ultramanager.UltraManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

public class UServer {

    private String serverName;
    private UUID uuid;
    private Integer maxPlayers;
    private Motd motd;

    public UServer(){
        String serverName = this.loadServerName();
        this.uuid = UUID.nameUUIDFromBytes(serverName.getBytes());
        this.maxPlayers = 20;
        this.motd = new Motd("A Minecraft UServer","Using UltraManager");
    }

    public UServer(UUID uuid, String serverName, Integer maxPlayers, Motd motd){
        this.uuid = uuid;
        this.serverName = serverName;
        this.maxPlayers = maxPlayers;
        this.motd = motd;
    }

    public UServer(String serverName){
        this.serverName = serverName;
        this.uuid = UUID.nameUUIDFromBytes(serverName.getBytes());
        this.maxPlayers = 20;
        this.motd = new Motd("A Minecraft UServer","Using UltraManager");
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public void setMotd(Motd motd) {
        this.motd = motd;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerName() {
        return serverName;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public Motd getMotd() {
        return motd;
    }

    private String loadServerName(){
        Properties prop = new Properties();
        InputStream inputStream = null;
        try{
            inputStream = new FileInputStream(new File("/plugins", "server.properties"));
            prop.load(inputStream);
            return prop.getProperty("server-name");
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            try{
                inputStream.close();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

        return "MCServer with UltraManager";
    }

    public String serialize(){
        return this.getUuid().toString()+"~"+this.getServerName()+"~"+this.getMaxPlayers().toString()+"~"+this.getMotd().serialize();
    }

    public static UServer deserialize(String serialized){
        String[] s = serialized.split("~");
        UUID uuid = UUID.fromString(s[0]);
        String svName = s[1];
        Integer mxPlyrs = Integer.parseInt(s[2]);
        Motd motd = Motd.deserialize(s[3]);
        return new UServer(uuid, svName, mxPlyrs, motd);
    }

    public void save(){ UltraManager.i.saveServer(this); }
}
