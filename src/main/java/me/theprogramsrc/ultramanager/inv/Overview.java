package me.theprogramsrc.ultramanager.inv;

import me.theprogramsrc.tpsapi.gameManagement.inventory.*;
import me.theprogramsrc.tpsapi.gameManagement.inventory.content.*;
import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.object.User;
import org.bukkit.entity.Player;

public class Overview extends SimpleInventory {

    public Overview(User user){
        // Load the inventory
        super(UltraManager.tps);
        this.loadUI(user.getPlayer());
    }

    //Load the contents
    @Override
    public void onUILoad(Player player, InventoryContents contents){
        contents.set(new SlotPos(3,1), this.getUserBrowser());
        contents.set(new SlotPos(3,1), this.getAddAdminItem());
        contents.set(new SlotPos(1, 3), this.getServerManagementItem());
    }

    //Return the inventory Internal Name
    @Override
    public String getInternalName() {
        return "Overview";
    }

    //Return the inventory rows
    @Override
    public Integer getRows() {
        return 3;
    }

    //Return if the inventory is closeable
    @Override
    public boolean isCloseable() {
        return true;
    }


    //Return title of inventory
    @Override
    public String getTitle() {
        return this.getLanguage().equalsIgnoreCase("es") ? "&7> &8Vista Previa" : "&7> &8Overview";
    }

    public ClickableItem getServerManagementItem(){
        //Create  server management item
        UItem item = new UItem(UMaterial.CHEST);
        item.setDisplayName(this.getLanguage().equalsIgnoreCase("es") ? "&aServidor" : "&aServer");
        if(this.getLanguage().equalsIgnoreCase("es")){
            item.setLore("","&7Editar servidor");
        }else{
            item.setLore("","&7Edit server");
        }
        item.setAmount(1);

        //Create ClickableItem and open the ServerManagementUI
        ClickableItem clickableItem = new ClickableItem(item, e->{
            new ServerManagementUI(UltraManager.i.getUser(((Player)e.getWhoClicked())), UltraManager.i.getUServer());
        });
        return clickableItem;
    }

    public ClickableItem getUserBrowser(){
        //Set random non admin user;
        User u = UltraManager.i.getRandomUser();
        while(u.isAdmin()){
            u = UltraManager.i.getRandomUser();
        }
        //Create add admin item
        UItem item = new UItem(UMaterial.PLAYER_HEAD).setSkullOwner(u.getName());
        item.setDisplayName(this.getLanguage().equalsIgnoreCase("es") ? "&aBuscador de Usuarios" : "&aUser Browser");
        if(this.getLanguage().equalsIgnoreCase("es")){
            item.setLore("","&7Abrir buscador de usuarios");
        }else{
            item.setLore("","&7Open the User Browser");
        }

        item.setAmount(1);
        //Create clickable item, open the user browser and open add admin ui
        ClickableItem clickableItem = new ClickableItem(item, e-> new UserBrowser(UltraManager.i.getUser(e.getWhoClicked().getUniqueId()), UserUI::new));
        return clickableItem;
    }

    public ClickableItem getAddAdminItem(){
        //Create add admin item
        UItem item = new UItem(UMaterial.PLAYER_HEAD).setSkullOwner(UltraManager.i.getRandomUser().getName());
        item.setDisplayName(this.getLanguage().equalsIgnoreCase("es") ? "&aAgregar admin" : "&aAdd admin");
        if(this.getLanguage().equalsIgnoreCase("es")){
            item.setLore("","&7Click para agregar a un administrador");
        }else{
            item.setLore("","&7Click to add an admin");
        }

        item.setAmount(1);
        //Create clickable item, open the user browser and open add admin ui
        ClickableItem clickableItem = new ClickableItem(item, e-> new UserBrowser(UltraManager.i.getUser(e.getWhoClicked().getUniqueId()), UserUI::new));
        return clickableItem;
    }
}
