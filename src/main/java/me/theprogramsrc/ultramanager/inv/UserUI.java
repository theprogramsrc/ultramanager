package me.theprogramsrc.ultramanager.inv;

import me.theprogramsrc.tpsapi.gameManagement.chat.SDialog;
import me.theprogramsrc.tpsapi.gameManagement.inventory.ClickableItem;
import me.theprogramsrc.tpsapi.gameManagement.inventory.SimpleInventory;
import me.theprogramsrc.tpsapi.gameManagement.inventory.UItem;
import me.theprogramsrc.tpsapi.gameManagement.inventory.UMaterial;
import me.theprogramsrc.tpsapi.gameManagement.inventory.content.InventoryContents;
import me.theprogramsrc.tpsapi.gameManagement.inventory.content.SlotPos;
import me.theprogramsrc.tpsapi.programmingUtils.Utils;
import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.object.User;
import org.bukkit.entity.Player;

public class UserUI extends SimpleInventory {

    private User user;

    public UserUI(User var0, User var1){
        super(UltraManager.tps);
        this.user = var1;
        this.loadUI(var0.getPlayer());
    }

    @Override
    public String getInternalName() {
        return "UserUI";
    }

    @Override
    public String getTitle() {
        return "&7>&5 "+this.user.getNick();
    }

    @Override
    public Integer getRows() {
        return 2;
    }

    @Override
    public boolean isCloseable() {
        return true;
    }

    @Override
    public void onUILoad(Player player, InventoryContents contents) {
        contents.set(new SlotPos(1,1), this.setNickItem());
        contents.set(new SlotPos(this.getRows(), 9), this.getBackItem());
    }


    public ClickableItem getBackItem(){ return new ClickableItem(new UItem(UMaterial.PLAYER_HEAD).setSkullOwner("MHF_ArrowLeft").setAmount(1), e-> new UserBrowser(this.user, UserUI::new)); }

    public ClickableItem setNickItem(){
        //Create set nick item
        UItem item = new UItem(UMaterial.PAPER);
        item.setDisplayName("&aNick");
        if(this.getLanguage().equalsIgnoreCase("es")){
            item.setLore("","&7Colocar nick","&7Nick: &b"+this.user.getNick());
        }else{
            item.setLore("","&7Set nick","&7Nick: &b"+this.user.getNick());
        }

        //Create click action opening a dialog to set the nick
        ClickableItem clickableItem = new ClickableItem(item, e->{
            new SDialog(getTPS(), ((Player)e.getWhoClicked()), "SetNick", "&bNick", (this.getLanguage().equalsIgnoreCase("es") ? "&7Colocar Nick" : "&7Set nick"), (this.getLanguage().equalsIgnoreCase("es") ? "&aPuedes usar codigos de color, Ej: &6TheProgramSrc" : "&aYou can use Color Codes, Example: &6TheProgramSrc")){
                @Override
                public boolean onPlayerInput(String s) {
                    if(s.length() <= 8){
                        Utils.sendMessage(e.getWhoClicked(), UserUI.this.getLanguage().equalsIgnoreCase("es") ? getTPS().getPrefix()+"&c&lDebe ser un nick entre 8 y 25 caracteres!" : getTPS().getPrefix()+"&c&lNeed to be a nick between 8 and 25 characters!");
                        new UserUI(UltraManager.i.getUser(e.getWhoClicked().getUniqueId()), UserUI.this.user);
                    }else{
                        String nick = s;
                        User u = UserUI.this.user;
                        u.setNick(nick);
                        u.save();
                        Utils.sendMessage(e.getWhoClicked(), UserUI.this.getLanguage().equalsIgnoreCase("es") ? getTPS().getPrefix()+"&aNick actualizado!" : getTPS().getPrefix()+"&aNick updated!");
                        new UserUI(UltraManager.i.getUser(e.getWhoClicked().getUniqueId()), UserUI.this.user);
                    }
                    return true;
                }
            };

        });
        return clickableItem;
    }
}
