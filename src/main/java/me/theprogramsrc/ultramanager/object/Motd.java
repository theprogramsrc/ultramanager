package me.theprogramsrc.ultramanager.object;

public class Motd {

    private String line1;
    private String line2;

    public Motd(String line1, String line2){
        this.line1 = line1;
        this.line2 = line2;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine1() {
        return line1;
    }

    public String getLine2() {
        return line2;
    }

    public String serialize(){
        return "[Motd:"+this.getLine1()+"α"+this.getLine2()+"]";
    }

    public static Motd deserialize(String serialized){
        String s = serialized.replace("[", "");
        s = s.replace("]", "");
        s = s.replaceAll("Motd:","");
        String[] motd = s.split("α");
        return new Motd(motd[0],motd[1]);
    }
}
