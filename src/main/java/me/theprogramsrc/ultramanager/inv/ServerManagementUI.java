package me.theprogramsrc.ultramanager.inv;

import me.theprogramsrc.tpsapi.gameManagement.chat.SDialog;
import me.theprogramsrc.tpsapi.gameManagement.inventory.*;
import me.theprogramsrc.tpsapi.gameManagement.inventory.content.InventoryContents;
import me.theprogramsrc.tpsapi.gameManagement.inventory.content.SlotPos;
import me.theprogramsrc.tpsapi.programmingUtils.Utils;
import me.theprogramsrc.ultramanager.object.UServer;
import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.object.User;
import org.bukkit.entity.Player;

public class ServerManagementUI extends UpdatableInventory {

    private UServer UServer;

    public ServerManagementUI(User user, UServer UServer){
        super(UltraManager.tps);
        this.UServer = UServer;
        this.loadUI(user.getPlayer());
    }

    @Override
    public String getInternalName() {
        return "ServerManagement";
    }

    @Override
    public String getTitle() {
        return "&7>&5 UServer";
    }

    @Override
    public Integer getRows() {
        return 3;
    }

    @Override
    public boolean isCloseable() {
        return true;
    }

    @Override
    public void onUILoad(Player player, InventoryContents contents) {
        contents.set(new SlotPos(1,1), this.setServerNameItem());
        contents.set(new SlotPos(1,2), this.setServerMaxPlayersItem());
        contents.set(new SlotPos(this.getRows(), 9), this.getBackItem());
    }

    @Override
    public void onUIUpdate(Player player, InventoryContents contents) {
        contents.set(new SlotPos(1,1), this.setServerNameItem());
        contents.set(new SlotPos(1,2), this.setServerMaxPlayersItem());
    }

    public ClickableItem setServerNameItem(){
        //Create SetServerName Item
        UItem item = new UItem(UMaterial.PAPER);
        item.setDisplayName(this.getLanguage().equalsIgnoreCase("es") ? "&aNombre del Servidor" : "&aServer Name");
        if(this.getLanguage().equalsIgnoreCase("es")){
            item.setLore("","&7Click para cambiar el nombre del servidor", "&7Nombre: &b"+this.UServer.getServerName());
        }else{
            item.setLore("","&7Click to change the UServer Name", "&7Name: &b"+this.UServer.getServerName());
        }

        //Create clickable item, open a dialog and set the sv name
        ClickableItem clickableItem = new ClickableItem(item, e->{
            new SDialog(getTPS(), ((Player)e.getWhoClicked()), "SetServerName", (this.getLanguage().equalsIgnoreCase("es") ? "&bNombre" : "&bName"), (this.getLanguage().equalsIgnoreCase("es") ? "&7Escribe un nombre" : "&7Type in a name") , (this.getLanguage().equalsIgnoreCase("es") ? "&7Ejemplo: &aTheProgramSrc UServer" : "&7Example: &aTheProgramSrc UServer")){
                @Override
                public boolean onPlayerInput(String s) {
                    if(ServerManagementUI.this.getLanguage().equalsIgnoreCase("es")){
                        ServerManagementUI.this.UServer.setServerName(s);
                        ServerManagementUI.this.UServer.save();
                        Utils.sendMessage(e.getWhoClicked(), "&aNombre cambiado");
                    }else{
                        ServerManagementUI.this.UServer.setServerName(s);
                        ServerManagementUI.this.UServer.save();
                        Utils.sendMessage(e.getWhoClicked(), "&aChanged name");
                    }
                    new ServerManagementUI(UltraManager.i.getUser(((Player)e.getWhoClicked())), ServerManagementUI.this.UServer);
                    return true;
                }
            };
        });
        return clickableItem;
    }

    public ClickableItem setServerMaxPlayersItem(){
        //Create item
        UItem item = new UItem(UMaterial.PLAYER_HEAD);
        item.setDisplayName(this.getLanguage().equalsIgnoreCase("es") ? "&aJugadores Maximos" : "&aMax Players");
        if(this.getLanguage().equalsIgnoreCase("es")){
            item.setLore("","&7Coloca los jugadores maximos", "&7Jugadores Maximos: &b"+this.UServer.getMaxPlayers().toString());
        }else{
            item.setLore("","&7Set max players", "&7Max Players: &b"+this.UServer.getMaxPlayers().toString());
        }

        //Create clickable item and open a dialog to set the max players
        ClickableItem clickableItem = new ClickableItem(item, e->{
            new SDialog(getTPS(), ((Player)e.getWhoClicked()), "SetMaxPlayers", (this.getLanguage().equalsIgnoreCase("es") ? "&bJugadores Maximos" : "&bMax Players"), (this.getLanguage().equalsIgnoreCase("es") ? "&7Coloca los jugadores maximos" : "&7Set max players"), (this.getLanguage().equalsIgnoreCase("es") ? "&7Ejemplo: &520" : "&7Example: &520")){
                @Override
                public boolean onPlayerInput(String s) {
                    if(Utils.isInteger(s)){
                        if(ServerManagementUI.this.getLanguage().equalsIgnoreCase("es")){
                            ServerManagementUI.this.UServer.setMaxPlayers(Integer.parseInt(s));
                            ServerManagementUI.this.UServer.save();
                            Utils.sendMessage(e.getWhoClicked(), "&aJugadores Maximos cambiados");
                        }else{
                            ServerManagementUI.this.UServer.setMaxPlayers(Integer.parseInt(s));
                            ServerManagementUI.this.UServer.save();
                            Utils.sendMessage(e.getWhoClicked(), "&aChanged Max Players");
                        }
                    }else{
                        if(ServerManagementUI.this.getLanguage().equalsIgnoreCase("es")){
                            Utils.sendMessage(e.getWhoClicked(), "&c&lDebes escribir un numero!");
                        }else{
                            Utils.sendMessage(e.getWhoClicked(), "&c&lYou need to write a number!");
                        }
                    }
                    new ServerManagementUI(UltraManager.i.getUser(((Player)e.getWhoClicked())), ServerManagementUI.this.UServer);
                    return true;
                }
            };
        });
        return clickableItem;
    }


    public ClickableItem getBackItem(){ return new ClickableItem(new UItem(UMaterial.PLAYER_HEAD).setSkullOwner("MHF_ArrowLeft").setDisplayName("&a<--").setAmount(1), e-> new ServerManagementUI(UltraManager.i.getUser(((Player)e.getWhoClicked())), this.UServer)); }

}
