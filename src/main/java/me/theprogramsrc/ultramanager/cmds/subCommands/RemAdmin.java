package me.theprogramsrc.ultramanager.cmds.subCommands;

import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.object.User;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RemAdmin extends UMCSubCmd{

    public RemAdmin(){
        super("remAdmin", "remAdmin <Player>");
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if(args.length == 0){
            return false;
        }else{
            Player p = Bukkit.getPlayer(args[0]);
            if(p != null){
                User u = UltraManager.i.getUser(p);
                if(u != null){
                    u.setAdmin(false);
                    u.save();
                }else{
                    u = new User(p);
                    u.setAdmin(false);
                    u.save();
                }
                return true;
            }else{
                return false;
            }
        }
    }
}
