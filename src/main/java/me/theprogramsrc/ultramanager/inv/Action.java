package me.theprogramsrc.ultramanager.inv;

import me.theprogramsrc.ultramanager.object.User;

public interface Action {

    void run(User viewer, User selected);
}
