package me.theprogramsrc.ultramanager.cmds.subCommands;

import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.object.User;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddAdmin extends UMCSubCmd{

    public AddAdmin(){
        super("addAdmin", "addAdmin <Player>");
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if(args.length == 0){
            return false;
        }else{
            Player p = Bukkit.getPlayer(args[0]);
            if(p != null){
                User u = UltraManager.i.getUser(p);
                if(u != null){
                    u.setAdmin(true);
                    u.save();
                }else{
                    u = new User(p);
                    u.setAdmin(true);
                    u.save();
                }
                return true;
            }else{
                return false;
            }
        }
    }
}
