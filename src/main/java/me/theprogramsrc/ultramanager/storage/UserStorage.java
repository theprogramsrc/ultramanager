package me.theprogramsrc.ultramanager.storage;

import me.theprogramsrc.tpsapi.generalManagement.files.CustomFile.CustomFile;
import me.theprogramsrc.ultramanager.UltraManager;
import me.theprogramsrc.ultramanager.object.User;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

public class UserStorage extends CustomFile{

    private int i = 0;

    public UserStorage(){ super(UltraManager.tps,false,false); }

    @Override
    protected void onFileLoad(){
        this.add(new User(UUID.fromString("9c08c6c2-37b6-4142-a6af-8699d3d90f61"), false, "TheProgramSrc"));
        this.save();
    }

    @Override
    protected void onFileReload(){
        this.i = 0;
        this.i = this.getKeys(false).size();
    }

    public void add(User user){
        String i = String.valueOf(this.i);
        this.set(i, user.serialize());
        this.i++;
        this.save();
    }

    public void save(final User user){
        this.getKeys(false).forEach(i->{
            if(User.deserialize(this.getString(i)).equals(user)){
                this.set(i,user.serialize());
            }
        });
    }

    public void rem(User user){
        this.getKeys(false).forEach(i->{
            if(User.deserialize(this.getString(i)).equalsIgnoreCase(user)){
                this.remove(i);
            }
        });
    }

    public User getUser(UUID uuid){
        User u = null;
        for(String s : this.getKeys(false)){
            if(User.deserialize(this.getString((s))).getUuid().equals(uuid)){
                u = User.deserialize(this.getString((s)));
            }
        }
        return u;
    }

    public User[] getUsers(){
        ArrayList usersArray = new ArrayList();
        this.getKeys(false).forEach(i->{
            User u = User.deserialize(this.getString(i));
            usersArray.add(u);
        });
        return ((User[])usersArray.toArray(new User[usersArray.size()]));
    }

    @Override
    protected File getFileFolder(){ return this.getDataFolder(); }

    @Override
    protected String getFileName(){ return "Users"; }

    @Override
    protected String getFileExtension(){ return ".yml"; }
}